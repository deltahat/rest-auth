from rest_framework import serializers
from . import models

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.CustomUser
        fields = ('email', 'username', )


class OperationSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Operation
        fields = ('operation', )
