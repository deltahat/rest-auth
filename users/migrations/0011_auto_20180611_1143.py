# Generated by Django 2.0.6 on 2018-06-11 11:43

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0010_auto_20180611_1130'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customuser',
            name='operation',
            field=models.OneToOneField(null=True, on_delete=django.db.models.deletion.CASCADE, to='users.Operation'),
        ),
    ]
