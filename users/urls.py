from django.urls import include, path
from django.conf.urls import url

from . import views

urlpatterns = [
    path('rest-auth/operations/', views.OperationListView.as_view()),
]
